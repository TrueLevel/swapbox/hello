README
======

Swap box is a free software (AGPL) project for serverless & exchangeless crypto cash machines.  

![3D Render!](/images/swapbox1.jpeg "3D Render")

## Software Repositories
- [gui](gitlab.com/TrueLevel/swapbox/gui)
  Swap box Kivy frontend
- ~~[swap-box-web3](github.com/TrueLevelSA/swap-box-web3)
  Web3x bridge, accessible through ZMQ~~ (deprecated)
- ~~[swap-box-contract](github.com/TrueLevelSA/swap-box-contract)
  Smart contract implementation and tests~~ (deprecated)
- ~~[swap-box-admin](github.com/TrueLevelSA/swap-box-admin)
  Smart contract deployment~~ (deprecated)
- [swap-box-zinc](gitlab.com/TrueLevel/swapbox/swapbox-zinc)
  Zinc (zkSync) smart contract implementation and tests
- [zksync-connector](gitlab.com/TrueLevel/swapbox/zksync-connector) (awaitng zksync-rs new release)
  zkSync/zkPorter bridge, accessible through ZMQ


## Hardware Repositories

- [swap-box-enclosure](https://github.com/TrueLevelSA/swap-box-enclosure)
 Prototype enclosure by TrueLevel SA
- [tola](https://gitlab.com/atola/swapboxenclosure/tola)
 First metal box by Atola
- [mini-swap](https://gitlab.com/atola/swapboxenclosure/mini-swap)
 Mini cash in design by Atola
-  [kumara](https://gitlab.com/atola/swapboxenclosure/kurmara)
 Latest design by Atola

For all TrueLevel SA Swap-Box repositories please [see our gitlab](https://gitlab.com/TrueLevel/swap-box)


High Level Overview
-------------------

### Buy transaction (cash-in)

```mermaid
sequenceDiagram
    Customer->>Machine: Insert cash 
Note over Machine, SmartContract: Single tx (tx.origin = machine address)
    Machine->>SmartContract: Send signed message/tx
    SmartContract->>DEX: tx (fiat amount less fee)
    DEX->>Customer: ETH tx 
```


### Sell transaction (cash-out)

```mermaid
sequenceDiagram
    Customer-->>Machine: Request cash
    Machine-->>Customer: Display payment address (QR) 
Note over Machine, SmartContract: Single tx (tx.origin = machine address)
    Machine->>SmartContract: Send signed message/tx
    SmartContract->>DEX: tx (ETH/token amount)
    DEX->>SmartContract: stablecoin tx
    SmartContract->Machine: Pay Cash (Event) less fees
    Machine->Customer: Dispense Cash
```
